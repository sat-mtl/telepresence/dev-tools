FROM alpine:3

# copies hadolint binary
# hadolint ignore=DL3022
COPY --from=hadolint/hadolint:latest-alpine \
    /bin/hadolint /bin/hadolint

# copies tasks in the bin folder
COPY tasks /bin

# installs all dependencies
RUN apk update \
 && apk add --no-cache --upgrade \
    aspell \
    bash \
    clang-extra-tools \
    curl \
    git \
    npm \
    py3-pip \
    shellcheck \
 && npm install -g \
    standard \
    stylelint \
    stylelint-scss \
    stylelint-config-standard-scss \
    markdownlint-cli \
 && pip3 install --break-system-packages --no-cache-dir \
    autopep8 \
    awscli \
    flake8
