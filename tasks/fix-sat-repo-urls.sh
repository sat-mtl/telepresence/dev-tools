#!/bin/bash

### Fix new new shmdata path
#sat-metalab/shmdata
#sat-mtl/tools/shmdata

grep -R "sat-metalab/shmdata"
find . -type f -exec sed -i 's/sat-metalab\/shmdata/sat-mtl\/tools\/shmdata/g' {} +
git add --all
git commit -m "Fix new shmdata path"

### Fix new new ndi2shmdata path
#sat-metalab/ndi2shmdata
#sat-mtl/tools/ndi2shmdata

grep -R "sat-metalab/ndi2shmdata"
find . -type f -exec sed -i 's/sat-metalab\/ndi2shmdata/sat-mtl\/tools\/ndi2shmdata/g' {} +
git add --all
git commit -m "Fix new ndi2shmdata path"

### Fix new new switcher path
#sat-metalab/switcher
#sat-mtl/tools/switcher

grep -R "sat-metalab/switcher"
find . -type f -exec sed -i 's/sat-metalab\/switcher/sat-mtl\/tools\/switcher/g' {} +
git add --all
git commit -m "Fix new switcher path"

### Fix new scenic-core path
#sat-mtl/telepresence/scenic-core
#sat-mtl/tools/scenic/scenic-core

grep -R "sat-mtl/telepresence/scenic-core"
find . -type f -exec sed -i 's/sat-mtl\/telepresence\/scenic-core/sat-mtl\/tools\/scenic\/scenic-core/g' {} +
git add --all
git commit -m "Fix new scenic-core path"

### Fix new scenicos path
#sat-mtl/telepresence/scenicos
#sat-mtl/distribution/scenicos

grep -R "sat-mtl/telepresence/scenicos"
find . -type f -exec sed -i 's/sat-mtl\/telepresence\/scenicos/sat-mtl\/distribution\/scenicos/g' {} +
git add --all
git commit -m "Fix new scenicos path"

### Fix new node-switcher path
#sat-mtl/telepresence/node-switcher
#sat-mtl/tools/scenic/node-switcher

grep -R "sat-mtl/telepresence/node-switcher"
find . -type f -exec sed -i 's/sat-mtl\/telepresence\/node-switcher/sat-mtl\/tools\/scenic\/node-switcher/g' {} +
git add --all
git commit -m "Fix new node-switcher path"

### Fix new dev-tools path
#sat-mtl/telepresence/dev-tools
#sat-mtl/valorisation/dev-tools

grep -R "sat-mtl/telepresence/dev-tools"
find . -type f -exec sed -i 's/sat-mtl\/telepresence\/dev-tools/sat-mtl\/valorisation\/dev-tools/g' {} +
git add --all
git commit -m "Fix new dev-tools path"

### Fix new scenic path
#sat-mtl/telepresence/scenic
#sat-mtl/tools/scenic/scenic

grep -R "sat-mtl/telepresence/scenic"
find . -type f -exec sed -i 's/sat-mtl\/telepresence\/scenic/sat-mtl\/tools\/scenic\/scenic/g' {} +
git add --all
git commit -m "Fix new scenic path"

### Fix new scenic-station path
#sat-mtl/telepresence/scenic-station
#sat-mtl/tools/scenic/scenic-station

grep -R "sat-mtl/telepresence/scenic-station"

find . -type f -exec sed -i 's/sat-mtl\/telepresence\/scenic-station/sat-mtl\/tools\/scenic\/scenic-station/g' {} +
git add --all
git commit -m "Fix new scenic-station path"

### Fix new sip-server path
#sat-mtl/telepresence/sip-server
#sat-mtl/tools/scenic/sip-server

grep -R "sat-mtl/telepresence/sip-server"
find . -type f -exec sed -i 's/sat-mtl\/telepresence\/sip-server/sat-mtl\/tools\/scenic\/sip-server/g' {} +
git add --all
git commit -m "Fix new sip-server path"

### Fix new scenic-selenium path
#sat-mtl/telepresence/scenic-selenium
#sat-mtl/tools/scenic/scenic-selenium

grep -R "sat-mtl/telepresence/scenic-selenium"
find . -type f -exec sed -i 's/sat-mtl\/telepresence\/scenic-selenium/sat-mtl\/tools\/scenic\/scenic-selenium/g' {} +
git add --all
git commit -m "Fix new scenic-selenium path"


### Fix scenic doc webpage url
#https://sat-mtl.gitlab.io/telepresence/scenic
#https://sat-mtl.gitlab.io/tools/scenic/scenic

grep -R "sat-mtl.gitlab.io/telepresence/scenic"
find . -type f -exec sed -i 's/sat-mtl.gitlab.io\/telepresence\/scenic/sat-mtl.gitlab.io\/tools\/scenic\/scenic/g' {} +
git add --all
git commit -m "Fix scenic doc webpage url"

### fix dboxer path
grep -R "sat-mtl.gitlab.io/haptic/dboxer"
find . -type f -exec sed -i 's#haptic/dboxer#tools/haptic-floor/dboxer#g' {} +
git add --all
git commit -m "🐛 fix dboxer doc paths"

### fix telluriq path
grep -R "sat-mtl.gitlab.io/haptic/telluriq"
find . -type f -exec sed -i 's#haptic/telluriq#tools/haptic-floor/telluriq#g' {} +
git add --all
git commit -m "🐛 fix telluriq doc paths"

### fix internal-docs
grep -R "sat-mtl.gitlab.io/telepresence/internal-docs"
find . -type f -exec sed -i 's#telepresence/internal-docs#valorisation/internal-docs#g' {} +
git add --all
git commit -m "🐛 fix internal-docs paths"
