#!/bin/bash

# Validates commit messages
#
# Arguments:
#   Path to the commit message

FILE=$1
ERROR=false

#######################################
# Prints errors nicely
# Returns:
#   A red message
#######################################
echoError () {
  local red='\033[0;31m'
  local default='\033[0m'

  echo -e "Error: $red$1$default"
}

#######################################
# Checks linter command
# Arguments:
#   A command name
# Returns:
#   True if the command exists
#######################################
checkCommand () {
  command -v "$1" >/dev/null 2>&1
}

#######################################
# Sanitizes the lines of a file
# Returns:
#   Parsed lines
#######################################
msgLines () {
  < "$FILE" sed 's/^#.*//' \
    | sed '/./=' \
    | sed '/^$/{p;d}; N; s/^/     /; s/ *\(.\{6,\}\)\n/\1  /' \
    | sed -n '/./,$p'
}

#######################################
# Gets the first line of a file
#######################################
firstLine () {
  msgLines | head -1
}

#######################################
# Gets the second line of a file
#######################################
secondLine () {
  msgLines | head -2 | tail -1
}

#######################################
# Gets all lines of a file except the first and the second
#######################################
restLines () {
  msgLines | sed '1,2d'
}

#######################################
# These commit message validators ensure that commit messages respect the 50/72 rule
# (https://medium.com/@preslavrachev/what-s-with-the-50-72-rule-8a906f61f09c).

# Ensure that the first line of the commit message is between 10 and 50 characters
# long. A 10 characters minimum ensure the commit message isn't something useless
# like 'Bug fix'.
#######################################
checkFirstLine () {
  if firstLine | sed -n '/\(.\)\{18\}/p' | grep -q '.'; then
    echoError "The first line must be at least 10 characters long."
    firstLine
    ERROR=true
  fi

  if firstLine | sed -n '/\(.\)\{59\}/p' | grep -q '.'; then
    echoError 'The first line must be 50 characters or less.'
    firstLine
    ERROR=true
  fi
}

#######################################
# Ensures that the second line of the commit message is blank
#######################################
checkSecondLine() {
  if secondLine | grep -q '.'; then
    echoError 'The second line should be blank.'
    secondLine
    ERROR=true
  fi;
}

#######################################
# Ensures that each line of the rest of the message doesn't exceed 72 chars
#######################################
checkRestLines () {
  if restLines | sed -n '/\(.\)\{81\}/p' | grep -q '.'; then
    echoError 'Text should be word-wrapped at 72 characters.'
    restLines | sed -n '/\(.\)\{81\}/p' | grep '.'
    ERROR=true
  fi;
}

#######################################
# Checks for spelling errors in the message. Detected errors will be flagged in the terminal,
# but will NOT prevent the commit (to account for things like ticket numbers, file extensions, etc.
# that could be flagged as spelling errors).
#######################################
checkSpelling () {
  local words
  words="$(aspell --mode=email --add-email-quote='#' list < "$FILE" | sort -u)"

  if [ -n "$words" ]; then
    printf "\e[1;33m\nPossible spelling errors found in commit message:\n\n\e[0m\e[0;31m%s\n\e[0m\e[1;33m\nUse git commit --amend to change the message.\e[0m\n" "$words"
  fi
}

if checkCommand "aspell"; then
  checkFirstLine

  if [[ "$(< "$FILE" wc -l)" -ne 1 ]]; then 
    checkSecondLine
    checkRestLines
  fi
  
  checkSpelling
fi
  
if $ERROR; then
  echoError 'Aborting commit. :('
  exit 1
fi

exit 0
