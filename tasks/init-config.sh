#!/bin/bash

# Initialize the configs.
#
# Arguments:
#   All languages that needs to be configured (ex "py docker").
# Environment variables:
#   DEV_TOOLS: URL to the dev tools repository


set -e
set -u
set -o pipefail

: "${DEV_TOOLS:=https://gitlab.com/sat-mtl/valorisation/dev-tools/-/raw/main}"

LINTER_CONFIGS="$DEV_TOOLS/config"

#######################################
# Copy each linter configurations
# Arguments:
#   List of all selected options
#######################################
copyEachConfig () {
  local options="$1"

  for option in $options; do
    case "$option" in
      c) downloadConfig "clang-format" ".clang-format";;
      css) downloadConfig "stylelint.json" ".stylelintrc";;
      py) downloadConfig "tox.ini" "tox.ini";;
      docker) downloadConfig "hadolint.yaml" ".hadolint.yaml";;
      md) downloadConfig "markdownlint.yaml" ".markdownlint.yaml";;
      editor) downloadConfig "editorconfig" ".editorconfig";;
      git) downloadConfig "gitmessage" ".gitmessage";;
    esac
  done
}

#######################################
# Copy all configurations
# Arguments:
#   List of all selected options
#######################################
copyAllConfig () {
  copyEachConfig "c css py docker md editor git"
}

#######################################
# Downloads a configuration
# Arguments:
#   Name of the input configuration file
#   Name of the output configuration file
#######################################
downloadConfig () {
  curl -L "$LINTER_CONFIGS/$1" -o "$2"
}

if [ "$#" -eq "1" ]; then
  copyEachConfig "$1"
else
  echo "Warning: Downloading all linter configurations!"
  copyAllConfig
fi
