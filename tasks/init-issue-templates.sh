#!/bin/bash

# Initialize or update all issue templates in current folder
#
# Environment variables:
#   DEV_TOOLS: URL to the dev tools repository

: "${DEV_TOOLS:=https://gitlab.com/sat-mtl/valorisation/dev-tools/-/raw/main}"

REMOTE_TEMPLATES="$DEV_TOOLS/.gitlab/issue_templates"
LOCAL_TEMPLATES=".gitlab/issue_templates"

#######################################
# Downloads an issue templates
# Arguments:
#   Name of the issue template
#######################################
downloadIssueTemplate () {
  local escapedName="${1// /%20}"
  mkdir -p "$LOCAL_TEMPLATES"
  curl -L "$REMOTE_TEMPLATES/$escapedName" -o "$LOCAL_TEMPLATES/$1"
}

#######################################
# Download all issue templates
#######################################
copyAllIssueTemplates () {
  downloadIssueTemplate "Analyse Requirements.md"
  downloadIssueTemplate "Bug Resolution Request.md"
  downloadIssueTemplate "Feature Request.md"
  downloadIssueTemplate "Release Plan.md"
}

#######################################
# Copy each issue templates
# Arguments:
#   List of all selected options
#######################################
copyEachIssueTemplates () {
  local options="$1"

  for option in $options; do
    case "$option" in
      analyse) downloadIssueTemplate "Analyse Requirements.md";;
      bug) downloadIssueTemplate "Bug Resolution Request.md";;
      feature) downloadIssueTemplate "Feature Request.md";;
      release) downloadIssueTemplate "Release Plan.md";;
    esac
  done
}

if [ "$#" -eq "1" ]; then
  copyEachIssueTemplates "$1"
else
  echo "Warning: Downloading all issue templates!"
  copyAllIssueTemplates
fi
