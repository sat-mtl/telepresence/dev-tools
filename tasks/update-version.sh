#!/bin/bash
#
# Updates the version of a project.
# Arguments:
#   Version of the release.

set -e
set -u
set -o pipefail

#######################################
# Update version of package.json file.
# Arguments:
#   New version number.
#   Path to package.json
# Returns:
#   Print the updated file
#######################################
updatePackageJson () {
  local version="${1}"
  local path="${2}"

  sed -i 's#"version": ".*",$#"version": "'"${version}"'",#' "${path}"
}

#######################################
# Update version of CMakeLists.txt file.
# Arguments:
#   New version number.
#   Path to CMakeLists.txt file
# Returns:
#   Print the updated file
#######################################
updateCMakeListsTxt () {
  local version="${1}"
  local path="${2}"

  sed -i -r 's#project\((\w+) VERSION .*\)$#project(\1 VERSION '"${version}"')#g' "${path}"
}

#######################################
# Update version of pyproject.toml file.
# Arguments:
#   New version number.
#   Path to pyproject.toml and __init__.py files
# Returns:
#   Print the updated file
#######################################
updatePyProjectToml () {
  local version="${1}"
  local path="${2}"

  sed -i -r 's#(__)?version(__)? = ".*"$#\1version\1 = '"${version}"'#' "${path}"
}

#######################################
# Update version of shield badge.
# Arguments:
#   New version number.
#   Path to README.md
# Returns:
#   Print the updated file
#######################################
updateShield () {
  local version="${1}"
  local path="${2}"

  sed -i -r 's#-([0-9.]+)-#-'"${version}"'-#' "${path}"
}

if [ "$#" -ne 1 ]; then
  echo "VERSION argument is required"
  exit 0
else
  [[ -f package.json ]] && updatePackageJson "${1}" package.json
  [[ -f CMakeLists.txt ]] && updateCMakeListsTxt "${1}" CMakeLists.txt
  [[ -f pyproject.toml ]] && updatePyProjectToml "${1}" pyproject.toml
  [[ -f telluriq/__init__.py ]] && updatePyProjectToml "${1}" telluriq/__init__.py
  [[ -f README.md ]] && updateShield "${1}" README.md
fi
