#!/bin/bash
#
# Updates the CHANGELOG of a project.
# Arguments:
#   Name of the released project.
#   Version of the release.

set -e
set -u
set -o pipefail

#######################################
# Filters all merge and release commits.
# Returns:
#   Filtered commits.
#######################################
filterCommits () {
  grep -Ev "merge|[rR]elease"
}

#######################################
# Format the merge request commit by transforming the merge request reference into a link.
# Command description:
# 1. format the 'See merge request' message by the gitlab URL
# 2. format the merge request reference by the gitlab URL
# 3. transform the URL into a markdown link by copying the merge request number
# 4. permute the URL format (link)[title] to '[title](link)'
# Returns:
#   formated commits.
#######################################
formatCommit () {
  sed 's~See merge request ~(https://gitlab.com/~' \
    | sed 's~!~/-/merge_requests/~' \
    | sed -r 's~([0-9])+$~&)[!&]~' \
    | sed -r 's~\((.*)\)\[(.*)\]~([\2](\1))~'
}

#######################################
# Trims all commit messages.
# Returns:
#   A trimed message.
#######################################
trimCommit () {
  grep -v "^[[:space:]]*$" \
    | sed 's/ \+/ /g' \
    | sed '/[^)]$/{N;s/\n/ /}'
}

#######################################
# Transforms STDIN in markdown h2 title.
# Returns:
#   A markdown h2 as string.
#######################################
toMarkdownH2 () {
  sed -e 's/$/\n----------------------------------\n /'
}

#######################################
# Transforms STDIN in markdown li entry.
# Returns:
#   A markdown li as string.
#######################################
toMarkdownLi () {
  sed -e 's/^/\* /'
}

#######################################
# Fetches all commits from a specific date.
# Arguments:
#   Date to fetch commits from.
# Returns:
#   All commits filtered and trimed.
#######################################
fetchCommitsFrom () {
  git log --pretty=%b --decorate --since="${1}" \
    | formatCommit \
    | trimCommit
}

#######################################
# Fetches the latest git tag.
# Returns:
#   A git hash.
#######################################
fetchLastTag () {
  git describe --tags "$(git rev-list --tags --max-count=1)"
}

#######################################
# Fetches the date of the latest git tag.
# Returns:
#   A date.
#######################################
fetchLastTagDate () {
  git log -1 --format=%ai "$(fetchLastTag)"
}

#######################################
# Print the CHANGELOG title.
# Returns:
#   The CHANGELOG title.
#######################################
printChangelogTitle () {
  echo -e "Release Notes\n===================\n"
}

#######################################
# Fetches the CHANGELOG title..
# Returns:
#   The CHANGELOG title.
#######################################
fetchOldChangelog () {
  tail -n +3 "$1"
}

#######################################
# Prints a changelog for the new release.
# Arguments:
#   Name of the released project.
#   Version of the release.
# Outputs:
#   Writes new CHANGELOG to STDOUT.
#######################################
printNewChangelog () {
  local version="${2:-0.0.0}"
  local name="${1:-Scenic}"

  local now
  local title
  local body

  now="$(date "+%Y-%m-%d")"
  title="$(echo "${name}" "${version}" \("${now}"\) | toMarkdownH2)"
  body="$(fetchCommitsFrom "$(fetchLastTagDate)" | toMarkdownLi)"

  echo -e "${title}\n${body}"
}

#######################################
# Writes the full CHANGELOG with the new changes.
# Arguments:
#   New changelog part.
#   Path of the current
# Returns:
#   Writes full CHANGELOG in-place.
#######################################
generateFullChangelog () {
  local newChangelog
  local oldChangelog

  newChangelog="$(printNewChangelog "$1" "$2")"
  oldChangelog="$(fetchOldChangelog "$3")"

  printChangelogTitle > "$3"
  echo "$newChangelog" >> "$3"
  echo "$oldChangelog" >> "$3"
}

if [ "$#" -ne "2" ]; then
  echo "NAME and VERSION arguments are required"
  exit 0
else
  generateFullChangelog "$1" "$2" "CHANGELOG.md"
fi
