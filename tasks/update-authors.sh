#!/bin/bash
#
# Generates all authors from git commits.

set -e
set -u
set -o pipefail

#######################################
# Checks if an AUTHOR file exists.
# Returns:
#   The AUTHOR file path.
#######################################
checkAuthorFile () {
  local authors="AUTHORS.md"

  if ! [ -f "$authors" ]; then
    echo "no authors file found, exiting"
	exit
  fi

  echo authors
}

#######################################
# Parses and orders the authors by number of commits.
# Returns:
#   The AUTHORS list.
#######################################
generateAuthors () {
  git log --format='%aN' \
    | sed 's/Francois/François/' \
    | sed 's/François/François/' \
    | sed 's/Fran\]ois/François/' \
    | sed 's/ubald/François Ubald Brien/' \
    | sed 's/^chesnel$/Pierre-Antoine Chesnel/' \
    | sed 's/^Chesnel$/Pierre-Antoine Chesnel/' \
    | sed 's/^Arnaud$/Arnaud Grosjean/' \
    | sed 's/nicolas/Nicolas Bouillot/' \
    | sed 's/Nina/Nicolas Bouillot/' \
    | sed 's/Jeremie Soria/Jérémie Soria/' \
    | sed 's/francoisph/François Philippe/' \
    | sed 's/fphilippe/François Philippe/' \
    | sed 's/vlaurent/Valentin Laurent/' \
    | sed 's/vlnk/Valentin Laurent/' \
    | sed 's/flecavalier/Francis Lecavalier/' \
    | sed 's/serigne saliou dia/Serigne Saliou Dia/' \
    | sed 's/PascaleSat/Pascale Stark/' \
    | sed 's/Aurélien/Aurélien Perronneau/' \
    | sed 's/[Oo]p[sS]ocket/Aurélien Perronneau/' \
    | sed 's/ogauthier_sat/Olivier Gauthier/' \
    | sed 's/Michal Seta/Michał Seta/' \
    | grep -v metalab \
    | grep -v 4d3d3d3 \
    | grep -v Ubuntu \
    | sort -bg \
    | uniq -c \
    | sed 's/\ *[0-9]*\ //' \
    | sed 's/^/* /'
}

generateAuthors > "AUTHORS.md"
