#!/bin/bash

set -e
set -u
set -o pipefail

# Configures all the common project tools
#
# Environment variables:
#   DEV_TOOLS: URL to the dev tools repository

: "${DEV_TOOLS:=https://gitlab.com/sat-mtl/valorisation/dev-tools/-/raw/main}"

#######################################
# Checks linter command
# Arguments:
#   A command name
# Returns:
#   True if the command exists
#######################################
checkCommand () {
  command -v "$1" >/dev/null 2>&1
}

#######################################
# Prints errors nicely
# Returns:
#   A red message
#######################################
echoError () {
  local red='\033[0;31m'
  local default='\033[0m'

  echo -e "Error: $red$1$default"
}

#######################################
# Prints infos nicely
# Returns:
#   A blue message
#######################################
echoInfo () {
  local blue='\e[0;36m'
  local default='\033[0m'

  echo -e "Info: $blue$1$default"
}

#######################################
# Asks for the used languages
# Returns:
#   List of all selected options
#######################################
askUsedLanguages () {
  dialog \
    --backtitle "Some language don't require configuration" \
    --clear \
    --checklist \
    --stdout \
    "Used configurations:" 20 40 8 \
    c "ClangFormat (C/C++)" on \
    css "Stylelint (CSS)" off \
    py "Flake8 (Python)" on \
    docker "Hadolint (Docker)" off \
    md "Markdownlint" on \
    editor "EditorConfig" on \
    git "GitMessage" on
}

#######################################
# Asks what pre-commit hook it should install
# Arguments:
#   Title of the action
# Returns:
#   List of all selected options
#######################################
askPreCommitHooks () {
  dialog \
    --clear \
    --checklist \
    --stdout \
    "Install pre-commit hooks:" 20 40 8 \
    common "Shell scripts, Dockerfiles, Markdown" on \
    py "Flake8, Black, Isort" off \
    js "Standard, Stylelint" off \
    cpp "ClangFormat, ClangTidy" off \
    php "PHPStan" off
}

#######################################
# Asks if an action should be performed
# Arguments:
#   Title of the action
# Returns:
#   List of all selected options
#######################################
askAction () {
  dialog \
    --clear \
    --yesno \
    --stdout \
    "$1" 10 40
}

#######################################
# Updates all git hooks in current folder
# Depends on the initHooks.sh script
#######################################
initPreCommitHooks () {
  local title="Should we init or update pre-commit hooks?"

  if askAction "$title"; then
    options="$(askPreCommitHooks)"
    echoInfo "Generating the pre-commit configuration"
    curl -Ls "$DEV_TOOLS/tasks/init-hooks.sh" | bash -s "$options"
  fi
}

#######################################
# Updates all linter (and others) configuration in current folder
# Depends on the initLinterConfig.sh script
#######################################
initConfig () {
  local title="Should we init or update language configurations?"

  if askAction "$title"; then
    options="$(askUsedLanguages)"
    echoInfo "Downloading configurations for $options"
    curl -Ls "$DEV_TOOLS/tasks/init-config.sh" | bash -s "$options"
  fi
}

if checkCommand "dialog"; then
  initConfig
  initPreCommitHooks
else
  echoError "Dialog must be installed: sudo apt install dialog"
fi
